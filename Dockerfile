#For codestream

FROM ubuntu

LABEL maintainer "Chris McClanahan <mcclanahanc@vmware.com>"
LABEL description "Container with the vSphere K8s CLIs and TKG CLIs"

COPY ./vsphere-plugin.zip .
RUN apt update && \
    apt install -y wget && \
    apt install -y unzip && \
    apt install -y gzip && \
    apt install -y python3 && \
    apt install -y expect && \
    apt install -y curl && \
    apt install -y python3-pip && \
    pip3 install requests && \
    unzip vsphere-plugin.zip && \
    rm ./vsphere-plugin.zip && \
    mv ./bin/kubectl-vsphere /usr/local/bin/kubectl-vsphere && \
    mv ./bin/kubectl /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl-vsphere && \
    chmod +x /usr/local/bin/kubectl
